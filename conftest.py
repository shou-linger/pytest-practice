# -*- coding: utf-8 -*-
from typing import List
import pytest
from pytest_lianxi.pythoncalc.calc import Calculator

'''
pytest 用来存储公共资源的文件，保存一些公共数据,或者fixture，或者hook
名字是固定的
应用的时候也不需要导入
'''


#fixture方法，来实现获取calc，相当于setup+teardown
@pytest.fixture(scope='class')
def get_calc():
    #setup
    print("开始计算")
    calc = Calculator()
    #yield 前面部分相当于setup,
    yield calc  #相当于return
    #yield  执行完用例后会执行yield   后面部分，相当于teardown,
    print("结束计算")


# 收集并且修改测试用例
def pytest_collection_modifyitems(
        session: "Session", config: "Config", items: List["Item"]
) -> None:
    # items 就是测试用例
    for item in items:
        item.name = item.name.encode('utf-8').decode('unicode-escape')
        item._nodeid = item.nodeid.encode('utf-8').decode('unicode-escape')











