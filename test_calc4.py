# -*- coding: utf-8 -*-
import logging

import allure
import pytest
import yaml


#从conftest公共库里面调用fixture方法，来实现获取calc

def get_datas():
    with open('./datas/calc.yml',"r", encoding="utf-8") as f :
        datas = yaml.safe_load(f)
    return datas

def test_getdatas():
    print(get_datas())


#完成测试数据的数据驱动，从calc.yml来获取数据


#feature 添加功能信息
@allure.feature("计算器")
class TestCalc:
    #加法:正常情况，整数、为0、负数、大数
    #ids 为测试用例起个别名
    #story 添加子功能信息
    @allure.story("相加功能的正常情况")
    @pytest.mark.parametrize(['a','b','expect'],get_datas()['add_int']['datas'],ids=get_datas()['add_int']['ids'])
    def test_add_int(self,get_calc,a,b,expect):
        logging.info(f'正常情况的相加功能 {a} + {b} == {expect}')
        #a,b 相加的两个数
        #expect  预期结果
        assert expect == get_calc.add(a,b)


    #加法：小数情况  小数点+小数点 小数点+0  负的小数点+负的正整数  正整数+小数点
    @allure.story("相加功能的小数情况")
    @pytest.mark.parametrize(['a','b','expect'],get_datas()['add_float']['datas'],ids=get_datas()['add_float']['ids'])
    def test_add_float(self,get_calc,a,b,expect):
        logging.info(f'小数情况的相加功能 {a} + {b} == {expect}')
        #a,b 相加的两个数
        #expect  预期结果
        assert expect == round(get_calc.add(a,b),2)

    #加法：异常情况  字符串+字符串
    @allure.story("相加功能的异常情况 字符串+字符串")
    @pytest.mark.parametrize(['a','b','expect'],get_datas()['add_except']['datas'],ids=get_datas()['add_except']['ids'])
    def test_add_float(self,get_calc,a,b,expect):
        logging.info(f'异常情况的相加功能 {a} + {b} == {expect}')
        #a,b 相加的两个数
        #expect  预期结果
        assert expect == get_calc.add(a, b)


    #加法：异常情况  整数+字符串 小数点+字符串
    @allure.story("相加功能的异常情况 整数+字符串")
    def test_add_error1(self):
        with pytest.raises(TypeError):
            1+'a'

    @allure.story("相加功能的异常情况 小数点+字符串")
    def test_add_error2(self):
        with pytest.raises(TypeError):
            1.2+'a'



    #除法：正常情况，整数，负数，大数
    @allure.story("相除功能的正常情况")
    @pytest.mark.parametrize(['a', 'b', 'expect'], get_datas()['div_int']['datas'],ids=get_datas()['div_int']['ids'])
    def test_div_int(self, get_calc, a, b, expect):
        logging.info(f'相除功能的正常情况 {a}/{b} == {expect}')
        # a,b 相除的两个数
        # expect  预期结果
        assert expect == get_calc.div(a, b)


    #除法：小数情况  小数点/小数点   负的小数点/正整数  正整数/小数点
    @allure.story("相除功能的小数情况")
    @pytest.mark.parametrize(['a','b','expect'],get_datas()['div_float']['datas'],
                        ids=get_datas()['div_float']['ids'])
    def test_div_float(self,get_calc,a,b,expect):
        logging.info(f'小数情况的相除功能 {a}/{b} == {expect}')
        #a,b 相除的两个数
        #expect  预期结果
        assert expect == round(get_calc.div(a,b),2)


    #除法的异常情况  为0
    @allure.story("相除功能的异常情况")
    def test_div_error(self):
        #pytest异常处理机制，它会捕获预期的异常错误类型
        with pytest.raises(ZeroDivisionError):
            1/0

    @allure.story("相除功能的异常情况")
    def test_div_error1(self):
        # pytest异常处理机制，它会捕获预期的异常错误类型
        with pytest.raises(TypeError):
            'a' / 2

